﻿using MassTransit;
using System.Threading.Tasks;
using SharedModels;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoСodeConsumer : IConsumer<PromoСodeModel>
    {
         private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromoСodeConsumer(IRepository<Preference> preferencesRepository,
                                 IRepository<Customer> customersRepository,
                                 IRepository<PromoCode> promoCodesRepository)
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task Consume(ConsumeContext<PromoСodeModel> context)
        {
            var @event = context.Message;

            var preference = await _preferencesRepository.GetByIdAsync(@event.PreferenceId);

            if (preference == null)
                return;

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var request = new GivePromoCodeRequest();

            request.PartnerId = @event.PartnerId;
            request.PromoCodeId = @event.PromoCodeId;
            request.PromoCode = @event.PromoCode;
            request.ServiceInfo = @event.ServiceInfo;
            request.BeginDate = @event.BeginDate;
            request.EndDate = @event.EndDate;
            request.PreferenceId = preference.Id;

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
        }
    }
}
