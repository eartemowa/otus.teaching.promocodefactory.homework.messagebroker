﻿namespace SharedModels
{
    public class PromoСodeModel
    { 
        public Guid PromoCodeId { get; set; }
        public string PromoCode { get; set; }
        public string ServiceInfo { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public Guid PartnerId { get; set; }
        public string PartnerName { get; set; }
        public Guid PreferenceId { get; set; }
        public Guid PartnerManagerId { get; set; }
    }
}
