﻿using MassTransit;
using System.Threading.Tasks;
using SharedModels;
using System.Linq;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class AdministrationConsumer : IConsumer<PromoСodeModel>
    {
        private readonly IRepository<Employee> _employeeRepository;

        public AdministrationConsumer(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task Consume(ConsumeContext<PromoСodeModel> context)
        {
            var employee = await _employeeRepository.GetByIdAsync(context.Message.PartnerManagerId);

            if (employee == null)
                return;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
